<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Commands\WelcomeCommand;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer): void
    {
        $welcome = new WelcomeCommand();
        $welcome->execute($writer);
        $writer->writeln("Input your command. Use 'help' for the list of commands:");
        CommandsDirector::executeRound($this->random, $writer, $reader);
    }

    public function run(Reader $reader, Writer $writer): void
    {
        CommandsDirector::executeOnce($this->random, $writer, $reader);
    }
}
