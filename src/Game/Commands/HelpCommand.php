<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Commands\CommandInterface;

class HelpCommand implements CommandInterface
{
    public function execute(Writer $writer, Reader $reader = null, String $parameter = null)
    {
        $message = 'List of commands:' . PHP_EOL
            . 'help - shows this list of commands' . PHP_EOL
            . 'stats - shows stats of ship' . PHP_EOL
            . 'set-sail <east|west|north|south> - moves in given direction' . PHP_EOL
            . 'fire - attacks enemy\'s ship' . PHP_EOL
            . 'aboard - collect loot from the ship' . PHP_EOL
            . 'buy <strength|armour|luck|rum> - buys skill or rum: 1 chest of gold - 1 item' . PHP_EOL
            . 'drink - your captain drinks 1 bottle of rum and fill 30 points of health' . PHP_EOL
            . 'whereami - shows current harbor' . PHP_EOL
            . 'exit - finishes the game' . PHP_EOL;

        $writer->writeln($message);

    }

}
