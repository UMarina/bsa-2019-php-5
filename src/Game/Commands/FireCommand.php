<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Commands\CommandInterface;
use BinaryStudioAcademy\Game\GameDirector;


class FireCommand implements CommandInterface
{
    private $random;
    public function __construct($random)
    {
        $this->random = $random;
    }

    public function execute(Writer $writer, Reader $reader = null, String $parameter = null)
    {
        $message = GameDirector::getInstance()->fire($this->random);
        $writer->writeln($message);
    }
}