<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Commands\CommandInterface;

class WelcomeCommand implements CommandInterface
{
    public function execute(Writer $writer, Reader $reader = null, String $parameter = null)
    {
        $writer->writeln("  ########  #### ########     ###    ######## ########  ######   ");
        $writer->writeln("  ##     ##  ##  ##     ##   ## ##      ##    ##       ##    ##  ");
        $writer->writeln("  ##     ##  ##  ##     ##  ##   ##     ##    ##       ##        ");
        $writer->writeln("  ########   ##  ########  ##     ##    ##    ######    ######   ");
        $writer->writeln("  ##         ##  ##   ##   #########    ##    ##             ##  ");
        $writer->writeln("  ##         ##  ##    ##  ##     ##    ##    ##       ##    ##  ");
        $writer->writeln("  ##        #### ##     ## ##     ##    ##    ########  ######   ");

    }
}





