<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Commands\CommandInterface;
use BinaryStudioAcademy\Game\GameDirector;

class SetSailCommand implements CommandInterface
{
    public function execute(Writer $writer, Reader $reader = null, String $parameter = null)
    {
        $message = GameDirector::getInstance()->sail($parameter);
        $writer->writeln($message);
    }
}