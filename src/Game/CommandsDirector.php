<?php
namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class CommandsDirector
{
    public static function executeRound($random, Writer $writer, Reader $reader = null){

        while (!isset($command) || $command <> 'exit') {
            $input = trim($reader->read());
            if($input == 'exit') break;

            $pieces = explode(" ", $input);
            if (count($pieces) > 2) {
                $writer->writeln( "Too many options or wrong command, use 'help' command to get list of allowed commands");
            } else {
                $command = $pieces[0];
                $parameter = $pieces[1] ? $pieces[1] : '';
                if (!in_array($command, ['set-sail', 'buy']) && !empty($parameter)) {
                    $writer->writeln( "This command does not expect input parameters.");
                } else {
                    if(strpos($command, '-') === false) {
                        $className = ucfirst($command)."Command";
                    } else {
                        $command_parts = explode("-", $pieces[0]);
                        $className = ucfirst($command_parts[0]).ucfirst($command_parts[1])."Command";
                    }

                    $classCommand = "BinaryStudioAcademy\\Game\\Commands\\".$className;

                    if (class_exists($classCommand)) {
                        $comm = new $classCommand($random);
                        $comm->execute($writer, $reader, $parameter);
                    } else {
                        $writer->writeln( "Command '{$command}' not found, use 'help' command to get list of allowed commands");
                    }
                }
            }
        }
    }

    public static function executeOnce($random, Writer $writer, Reader $reader = null){

        $input = trim($reader->read());

        $pieces = explode(" ", $input);
        $command = $pieces[0];
        $parameter = $pieces[1] ? $pieces[1] : '';
        if(strpos($command, '-') === false) {
            $className = ucfirst($command)."Command";
        } else {
            $command_parts = explode("-", $pieces[0]);
            $className = ucfirst($command_parts[0]).ucfirst($command_parts[1])."Command";
        }

        $classCommand = "BinaryStudioAcademy\\Game\\Commands\\".$className;

        if (class_exists($classCommand)) {
            $comm = new $classCommand($random);
            $comm->execute($writer, $reader, $parameter);
        } else {
            $writer->writeln( "Command '{$command}' not found");
        }

    }
}