<?php

namespace BinaryStudioAcademy\Game\Contracts\Commands;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

interface CommandInterface
{
    public function execute(Writer $writer, Reader $reader = null, String $parameter = null);

}