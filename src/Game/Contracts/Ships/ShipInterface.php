<?php

namespace BinaryStudioAcademy\Game\Contracts\Ships;

interface ShipInterface
{
    public function getName() : string;

    public function getStat(string $name) : int;

    public function setStat(string $name, int $value) : void;

    public function getHold() : array;

    public function getShipParams() : array;

}