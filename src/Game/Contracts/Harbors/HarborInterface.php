<?php

namespace BinaryStudioAcademy\Game\Contracts\Harbors;

use BinaryStudioAcademy\Game\Contracts\Ships\ShipInterface;

interface HarborInterface
{
    public function getName() : string;

    public function getNumber() : int;

    public function getShipType() : string;

    public function getNeighbor($direction) : int;
}