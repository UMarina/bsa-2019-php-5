<?php

namespace BinaryStudioAcademy\Game\Abstracts;

use BinaryStudioAcademy\Game\Contracts\Ships\ShipInterface;

abstract class AbstractShip implements ShipInterface
{
    protected $name;
    protected $strength;
    protected $armour;
    protected $luck;
    protected $health;
    protected $hold = [];
    public $maxValues = [
        'strength' => 10,
        'armour' => 10,
        'luck' => 10,
        'health' => 100
    ];

    public function getName() : string
    {
        return $this->name;
    }

    public function getStat($name) : int
    {
        return $this->{$name};
    }

    public function setStat($name, int $value) : void
    {
        $this->{$name} = $value;
    }

    public function setDamage(int $value) : void
    {
        $this->health = max(0, ($this->health - $value));
    }

    public function getHold() : array
    {
        return $this->hold;
    }
    public function emptyHold() : void
    {
        $this->hold = [];
    }

    public function isAlive() : bool
    {
        if ($this->health > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getShipParams() : array
    {
        return [
            'name' => $this->name,
            'strength' => $this->strength,
            'armour' => $this->armour,
            'luck' => $this->luck,
            'health' => $this->health
        ];
    }
}

