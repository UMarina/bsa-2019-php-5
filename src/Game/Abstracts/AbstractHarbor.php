<?php

namespace BinaryStudioAcademy\Game\Abstracts;

use BinaryStudioAcademy\Game\Contracts\Harbors\HarborInterface;
use BinaryStudioAcademy\Game\Ships\ShipFactory;

abstract class AbstractHarbor implements HarborInterface
{
    protected $name;
    protected $number;
    protected $typeShip;
    protected $neighboringHarbors = [];
    protected $ship;


    public function getName() : string
    {
        return $this->name;
    }

    public function getNumber() : int
    {
        return $this->number;
    }

    public function getShipType() : string
    {
        return $this->typeShip;
    }

    public function getNeighbor($direction) : int
    {
        return $this->neighboringHarbors[$direction];

    }
    public function setShip() : void
    {
        $this->ship = ShipFactory::createShip($this->typeShip);

    }
    public function getShip() : AbstractShip
    {
        return $this->ship;

    }

}

