<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Abstracts\AbstractShip;

class ShipFactory
{
    public static function createShip($shipType) : AbstractShip
    {
        switch ($shipType) {
            case 'player':
                $ship = new PlayerShip();
                break;

            case 'schooner':
                $ship = new SchoonerShip();
                break;

            case 'battle':
                $ship = new BattleShip();
                break;

            case 'royal':
                $ship = new RoyalShip();
                break;
        }

        return $ship;
    }
}
