<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Abstracts\AbstractShip;

class RoyalShip extends AbstractShip
{
    public function __construct()
    {
        $this->name = 'HMS Royal Sovereign';
        $this->strength = 10;
        $this->armour = 10;
        $this->luck = 10;
        $this->health = 100;
        $this->hold = [
            'gold',
            'gold',
            'rum',
        ];

    }
}
