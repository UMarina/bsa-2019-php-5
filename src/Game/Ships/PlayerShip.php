<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Abstracts\AbstractShip;

class PlayerShip extends AbstractShip
{
    public function __construct()
    {
        $this->name = '';
        $this->strength = 4;
        $this->armour = 4;
        $this->luck = 4;
        $this->health = 60;
        $this->hold = [];
    }

    public function increaseStat(string $name) : bool
    {
        if ($this->{$name} < $this->maxValues[$name]) {
            $this->{$name}++;
            return true;
        } else {
            return false;
        }

    }
    public function decreaseStat(string $name) : bool
    {
        if ($this->{$name} > 0) {
            $this->{$name}--;
            return true;
        } else {
            return false;
        }
    }

    public function addToHold(string $type) : bool
    {
        if (count($this->hold) < 3) {
            if ($type === 'rum') {
                $this->hold[] = 'rum';
            } elseif ($type === 'gold') {
                $this->hold[] = 'gold';
            }
            return true;
        } else {
            return false;
        }
    }
    public function takeFromHold(string $type) : bool
    {
        if (in_array($type, $this->hold)) {
            $result=array_search($type,$this->hold,true);
            array_splice($this->hold, $result, 1);
            return true;
        } else {
            return false;
        }
    }
    public function lostBattle()
    {
        $this->strength--;
        $this->armour--;
        $this->luck--;
        $this->health = 60;
        $this->hold = [];
        if ($this->strength === 0 || $this->armour === 0 || $this->luck === 0) {
            exit("Game over.");
        }
    }
}