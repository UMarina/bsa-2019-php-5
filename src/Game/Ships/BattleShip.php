<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Abstracts\AbstractShip;

class BattleShip extends AbstractShip
{
    public function __construct()
    {
        $this->name = 'Royal Battle Ship';
        $this->strength = 8;
        $this->armour = 8;
        $this->luck = 7;
        $this->health = 80;
        $this->hold = [
            'rum',
        ];

    }
}
