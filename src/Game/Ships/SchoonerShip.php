<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Abstracts\AbstractShip;

class SchoonerShip extends AbstractShip
{
    public function __construct()
    {
        $this->name = 'Royal Patrool Schooner';
        $this->strength = 4;
        $this->armour = 4;
        $this->luck = 4;
        $this->health = 50;
        $this->hold = [
            'gold',
        ];

    }
}

