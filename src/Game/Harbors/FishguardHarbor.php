<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class FishguardHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Fishguard';
        $this->number = 3;
        $this->typeShip = 'schooner';
        $this->neighboringHarbors = [
            'east' => 1,
            'west' => 0,
            'north' => 4,
            'south' => 2
        ];
    }
}