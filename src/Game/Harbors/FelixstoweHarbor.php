<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class FelixstoweHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Felixstowe';
        $this->number = 7;
        $this->typeShip = 'battle';
        $this->neighboringHarbors = [
            'east' => 8,
            'west' => 2,
            'north' => 5,
            'south' => 0
        ];
    }
}