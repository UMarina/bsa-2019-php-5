<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class LondonDocksHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'London Docks';
        $this->number = 8;
        $this->typeShip = 'royal';
        $this->neighboringHarbors = [
            'east' => 0,
            'west' => 7,
            'north' => 6,
            'south' => 0
        ];
    }
}