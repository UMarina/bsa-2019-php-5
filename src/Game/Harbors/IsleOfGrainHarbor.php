<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class IsleOfGrainHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Isle of Grain';
        $this->number = 5;
        $this->typeShip = 'schooner';
        $this->neighboringHarbors = [
            'east' => 6,
            'west' => 4,
            'north' => 0,
            'south' => 7
        ];
    }
}