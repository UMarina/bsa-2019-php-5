<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class SouthhamptonHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Southhampton';
        $this->number = 2;
        $this->typeShip = 'schooner';
        $this->neighboringHarbors = [
            'east' => 7,
            'west' => 3,
            'north' => 1,
            'south' => 0
        ];
    }
}