<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class GraysHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Grays';
        $this->number = 6;
        $this->typeShip = 'battle';
        $this->neighboringHarbors = [
            'east' => 0,
            'west' => 5,
            'north' => 0,
            'south' => 8
        ];
    }
}