<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class HarborFactory
{
    public static function createHarbor($harborName) : AbstractHarbor
    {

        switch ($harborName) {
            case 'Pirates Harbor':
                $harbor = new PiratesHarbor();
                break;

            case 'Southhampton':
                $harbor = new SouthhamptonHarbor();
                break;

            case 'Fishguard':
                $harbor = new FishguardHarbor();
                break;

            case 'Salt End':
                $harbor = new SaltEndHarbor();
                break;

            case 'Isle of Grain':
                $harbor = new IsleOfGrainHarbor();
                break;

            case 'Grays':
                $harbor = new GraysHarbor();
                break;

            case 'Felixstowe':
                $harbor = new FelixstoweHarbor();
                break;

            case 'London Docks':
                $harbor = new LondonDocksHarbor();
                break;
        }

        return $harbor;
    }
}