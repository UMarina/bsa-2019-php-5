<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class SaltEndHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Salt End';
        $this->number = 4;
        $this->typeShip = 'schooner';
        $this->neighboringHarbors = [
            'east' => 5,
            'west' => 3,
            'north' => 0,
            'south' => 1
        ];
    }
}