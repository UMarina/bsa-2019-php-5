<?php

namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Abstracts\AbstractHarbor;

class PiratesHarbor extends AbstractHarbor
{
    public function __construct()
    {
        $this->name = 'Pirates Harbor';
        $this->number = 1;
        $this->typeShip = 'player';
        $this->neighboringHarbors = [
            'east' => 0,
            'west' => 3,
            'north' => 4,
            'south' => 2
        ];
    }
}