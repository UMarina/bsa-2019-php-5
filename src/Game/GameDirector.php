<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Harbors\HarborFactory;
use BinaryStudioAcademy\Game\Ships\ShipFactory;
use BinaryStudioAcademy\Game\Helpers\Math;

class GameDirector
{
    const MAP = [
        1 => 'Pirates Harbor',
        2 => 'Southhampton',
        3 => 'Fishguard',
        4 => 'Salt End',
        5 => 'Isle of Grain',
        6 => 'Grays',
        7 => 'Felixstowe',
        8 => 'London Docks'
    ];
    const DIRECTIONS = [
        'east',
        'west',
        'north',
        'south'
    ];

    private static $instance = null;
    private $harborsList = [];
    private $playerShip;
    private $currentHarbor;
    private $emojis = array(
        "gold" => "💰",
        "rum" => "🍾"
    );

    private function __construct()
    {
        foreach (self::MAP as $id => $harborName){
            $this->harborsList[$id] = HarborFactory::createHarbor($harborName);
        }
        $this->currentHarbor = $this->harborsList[1];
        $this->playerShip = ShipFactory::createShip($this->currentHarbor->getShipType());
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance(): GameDirector
    {
        if (is_null (self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function sail($direction)
    {
        if (!in_array($direction, self::DIRECTIONS)) {
            return "Direction '{$direction}' incorrect, choose from: east, west, north, south";
        } else {
            $id = $this->currentHarbor->getNeighbor($direction);
            if (!isset(self::MAP[$id])) {
                return "Harbor not found in this direction";
            } else {
                $this->currentHarbor = $this->harborsList[$id];
                $text = $this->getHarborParams() . '.' . PHP_EOL;
                if ($id != 1) {
                    $this->currentHarbor->setShip();
                    $ship = $this->currentHarbor->getShip()->getShipParams();
                    $text .= "You see {$ship['name']}: " . PHP_EOL
                    . 'strength: ' . $ship['strength'] . PHP_EOL
                    . 'armour: ' . $ship['armour'] . PHP_EOL
                    . 'luck: ' . $ship['luck']  . PHP_EOL
                    . 'health: ' . $ship['health']   . PHP_EOL;
                } elseif ($this->playerShip->getStat('health') < 60) {
                    $this->playerShip->setStat('health', 60);
                    $text .= 'Your health is repared to 60.' . PHP_EOL;
                }
                return $text;
            }
        }
    }

    public function getHarborParams()
    {
        return "Harbor " . $this->currentHarbor->getNumber(). ": " . $this->currentHarbor->getName();
    }

    public function getHoldStats()
    {
        $hold = $this->playerShip->getHold();
        $count_items = count($hold);
        $hold_text = '[ ';
        foreach ($hold as $item) {
            $hold_text .= $this->emojis[$item] . ' ';
        }
        if ($count_items < 3) {
            for ($i = 0; $i < (3 - $count_items); $i++){
                $hold_text .= '_ ';
            }
        }
        $hold_text .= ']';
        return $hold_text;
    }

    public function getPlayerStats()
    {
        return 'Ship stats:' . PHP_EOL
            . 'strength: ' . $this->playerShip->getStat('strength') . PHP_EOL
            . 'armour: ' . $this->playerShip->getStat('armour') . PHP_EOL
            . 'luck: ' . $this->playerShip->getStat('luck')  . PHP_EOL
            . 'health: ' . $this->playerShip->getStat('health')   . PHP_EOL
            . 'hold: ' . $this->getHoldStats() . PHP_EOL;
    }

    public function buy($item)
    {
        if (in_array($item,['rum', 'strength', 'armour', 'luck'])) {
            if ($this->playerShip->takeFromHold('gold')) {
                if ($item === 'rum') {
                    $this->playerShip->addToHold($item);
                    $hold = $this->playerShip->getHold();
                    $count_items = array_count_values($hold);
                    return "You\'ve bought a rum. Your hold contains {$count_items[$item]} bottle(s) of rum." . PHP_EOL;
                } else {
                    if ($this->playerShip->increaseStat($item)) {
                        return "You\'ve bought a {$item}. Your {$item} is {$this->playerShip->getStat($item)}." . PHP_EOL;
                    } else {
                        $this->playerShip->addToHold('gold');
                        return "Your {$item} can not be more than {$this->playerShip->getStat($item)} points." . PHP_EOL;
                    }
                }
            } else {
                return "Sorry, you do not have gold to buy {$item}!";
            }
        } else {
            return "Product '{$item}' incorrect, you can buy only: rum, strength, armour, luck";
        }
    }

     public function aboard()
     {
         if ($this->currentHarbor->getNumber() == 1) {
             return 'There is no ship to aboard';
         }

         if ($this->currentHarbor->getShip()->isAlive()) {
             return 'You cannot board this ship, since it has not yet sunk';
         } elseif (count($this->playerShip->getHold()) === 3) {
             return "Sorry, your hold is full!";
         } else {
             if (empty($this->currentHarbor->getShip()->getHold())) {
                 return "{$this->currentHarbor->getShip()->getName()} hold is empty";
             }
             if ($this->currentHarbor->getShipType() === 'schooner') {
                 $this->playerShip->addToHold('gold');
                 $this->currentHarbor->getShip()->emptyHold();
                 return 'You got 💰.' . PHP_EOL;
             } elseif ($this->currentHarbor->getShipType() === 'battle') {
                 $this->playerShip->addToHold('rum');
                 $this->currentHarbor->getShip()->emptyHold();
                 return 'You got 🍾.' . PHP_EOL;
             } else {
                 return 'There is no ship to aboard';
             }
         }
     }

    public function drink()
    {
        if (in_array('rum',$this->playerShip->getHold())) {
            if ($this->playerShip->getStat('health') < $this->playerShip->maxValues['health']){
                $this->playerShip->takeFromHold('rum');
                $this->playerShip->setStat('health', min(($this->playerShip->getStat('health') + 30), $this->playerShip->maxValues['health']));
                return "You\'ve drunk a rum and your health filled to {$this->playerShip->getStat('health')}";
            } else {
                return "Your health can not be more than 100 points.";
            }
        } else {
            return "There is no rum in your hold" . PHP_EOL
                . $this->getHoldStats() . PHP_EOL;
        }
    }
    public function fire($random)
    {
        if ($this->currentHarbor->getNumber() == 1 || !$this->currentHarbor->getShip()->isAlive()) {
            return 'There is no ship to fight';
        }

        $text = '';

        $playerLuckShot = Math::luck($random, $this->playerShip->getStat('luck'));

        if ($playerLuckShot) {
            $playerDamage = Math::damage($this->playerShip->getStat('strength'), $this->currentHarbor->getShip()->getStat('armour'));
            $this->currentHarbor->getShip()->setDamage($playerDamage);
            if ($this->currentHarbor->getShip()->isAlive()) {
                $text .= "{$this->currentHarbor->getShip()->getName()} has damaged on: {$playerDamage} points." . PHP_EOL
                    . "health: {$this->currentHarbor->getShip()->getStat('health')}" . PHP_EOL;
            } else {
                if ($this->currentHarbor->getNumber() === 8) {
                    return '🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾';
                }
                return "{$this->currentHarbor->getShip()->getName()} on fire. Take it to the boarding." . PHP_EOL;
            }
        } else {
            $text .= "Luck turned away from you. You missed.". PHP_EOL;
        }

        $enemyLuckShot = Math::luck($random, $this->currentHarbor->getShip()->getStat('luck'));

        if ($enemyLuckShot) {
            $enemyDamage = Math::damage($this->currentHarbor->getShip()->getStat('strength'), $this->playerShip->getStat('armour'));
            $this->playerShip->setDamage($enemyDamage);
            if ($this->playerShip->isAlive()) {
                $text .= "{$this->currentHarbor->getShip()->getName()} damaged your ship on: {$enemyDamage} points." . PHP_EOL
                    . "health: {$this->playerShip->getStat('health')}" . PHP_EOL;
            } else {
                $this->currentHarbor = $this->harborsList[1];
                $this->playerShip->lostBattle();
                $text .= "Your ship has been sunk." . PHP_EOL
                    . "You restored in the Pirate Harbor." . PHP_EOL
                    . "You lost all your possessions and 1 of each stats." . PHP_EOL;
            }
        } else {
            $text .= "Good luck on your side! {$this->currentHarbor->getShip()->getName()} missed.". PHP_EOL;
        }

        return $text;
    }
}